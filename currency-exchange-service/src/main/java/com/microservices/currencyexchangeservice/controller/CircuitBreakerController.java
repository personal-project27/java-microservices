package com.microservices.currencyexchangeservice.controller;

import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CircuitBreakerController {

    private Logger logger = LoggerFactory.getLogger(CircuitBreakerController.class);

    @GetMapping("/sample-api")
    @Retry(name = "sample-api", fallbackMethod = "hardcodedResponse")
    public String sampleApi() {
        logger.info("Sample Api call received");
        ResponseEntity<String> forEntity = new RestTemplate().getForEntity("http://localhost:8080/some-dummy-url", String.class);

        return forEntity.getBody();
    }

    //Volendo si può fare un metodo per ogni eccezione.
    public String hardcodedResponse(Exception ex) {
        return "Fallback Response";
    }

    @GetMapping("/circuit-breaker")
    //@CircuitBreaker(name = "default", fallbackMethod = "hardcodedResponseCircuit")
    //@RateLimiter(name = "default") //10 s -> 10000 calls to the circuit breaker
    @Bulkhead(name = "circuit-breaker")
    public String circuitBreaker() {
        //logger.info("Circuit Api call received");
        //ResponseEntity<String> forEntity = new RestTemplate().getForEntity("http://localhost:8080/some-dummy-url", String.class);

        return "ciao";
    }

    //Volendo si può fare un metodo per ogni eccezione.
    public String hardcodedResponseCircuit (Exception ex) {
        return "Fallback Response";
    }
}
